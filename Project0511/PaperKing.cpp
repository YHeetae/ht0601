#pragma warning(disable : 4996)

#include "PaperKing.h"
#include "Monster.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>


PaperKing::PaperKing(char *myname)
{
	paperKingName = new char[10];
	for (int i = 0; i < strlen(myname); i++) {
		strcpy(&paperKingName[i], &myname[i]);
	}
	paperKingHp = 100;
}


PaperKing::~PaperKing()
{
	delete[] paperKingName;
}

void PaperKing::attack() {
	srand(time(NULL));
	int ran = rand() % 100 + 1;
	if (0 < ran && 50 > ran) {
		char input[6] = "보";
		for (int i = 0; i < 6; i++) {
			strcpy(&paperKingResult[i], &input[i]);
		}
	}
	else if (50 <= ran && 75 > ran) {
		char input[6] = "바위";
		for (int i = 0; i < 6; i++) {
			strcpy(&paperKingResult[i], &input[i]);
		}
	}
	else if (75 <= ran && 101 > ran) {
		char input[6] = "가위";
		for (int i = 0; i < 6; i++) {
			strcpy(&paperKingResult[i], &input[i]);
		}
	}
}

void PaperKing::lose() {
	paperKingHp = paperKingHp - 20;
}
char* PaperKing::getResult() {
	return paperKingResult;
}
char* PaperKing::getName() {
	return paperKingName;
}