#pragma warning(disable : 4996)

#include "MyHero.h"
#include <iostream>
#include <cstdio>

using namespace std;

MyHero::MyHero(char myname[10], int myage, char myweapon[10], char mygender[3], int myAttack, int myDefence)
{
	for (int i = 0; i < 10; i++) {
		strcpy(&name[i], &myname[i]);
	}
	age = myage;
	for (int i = 0; i < 10; i++) {
		strcpy(&weapontype[i], &myweapon[i]);
	}
	level = 1;
	for (int i = 0; i < 3; i++) {
		strcpy(&gender[i], &mygender[i]);
	}
	hp = 100;
	attackPower = myAttack;
	defencePower = myDefence;
}


MyHero::~MyHero()
{
}

void MyHero::walk() {
	cout << name << "이 걷는다." << endl;
}
void MyHero::run() {
	cout << name << "이 뛴다." << endl;
}
void MyHero::fly() {
	cout << name<< "이 난다." << endl;
}
void MyHero::attack(MyHero otherHero) {
	cout << otherHero.name << "을 공격한다." << endl;
}
void MyHero::rest() {
	cout << name << "이 휴식을 취한다." << endl;
}
void MyHero::eat() {
	cout << name << "이 포션을 먹는다." << endl;
	hp = hp + 20;
}
void MyHero::levelup() {
	cout << name << "이 레벨업하였다." << endl;
	level++;
}
void MyHero::death() {
	cout << name << "이 사망하였다." << endl;
	if (level > 1) {
		level--;
	}
}
void MyHero::hit(MyHero otherHero) {
	cout << otherHero.name << "에게 공격당하였다." << endl;
	hp = hp - 20;
}

