#pragma warning(disable : 4996)

#include "ScissorKing.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>



ScissorKing::ScissorKing(char *myname)
{
	scissorKingName = new char[10];
	for (int i = 0; i < strlen(myname); i++) {
		strcpy(&scissorKingName[i], &myname[i]);
	}
	scissorKingHp = 100;
}


ScissorKing::~ScissorKing()
{
	delete[] scissorKingName;
}

void ScissorKing::attack() {
	srand(time(NULL));
	int ran = rand() % 100 + 1;
	if (0 < ran && 50 > ran) {
		char input[6] = "가위";
		for (int i = 0; i < 6; i++) {
			strcpy(&scissorKingResult[i], &input[i]);
		}
	}
	else if (50 <= ran && 75 > ran) {
		char input[6] = "가위";
		for (int i = 0; i < 6; i++) {
			strcpy(&scissorKingResult[i], &input[i]);
		}
	}
	else if (75 <= ran && 101 > ran) {
		char input[6] = "보";
		for (int i = 0; i < 6; i++) {
			strcpy(&scissorKingResult[i], &input[i]);
		}
	}
}
void ScissorKing::lose() {
	scissorKingHp = scissorKingHp - 20;
}
char* ScissorKing::getResult() {
	return scissorKingResult;
}
char* ScissorKing::getName() {
	return scissorKingName;
}