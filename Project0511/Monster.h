#pragma once
class Monster
{
protected:
	char *name;
	int hp;
	char result[6];
public:
	Monster();
	Monster(char *myname);
	~Monster();
	

	void attack();
	void lose();
	char* getResult();
	char* getName();
};

