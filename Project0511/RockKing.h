#pragma once
#include "Monster.h"

class RockKing : public Monster
{
private:
	char* rockKingname;
	int rockKingHp;
	char rockKingResult[6];
public:
	
	RockKing(char* myname);
	~RockKing();
	void attack();
	void lose();
	char* getResult();
	char* getName();
};

