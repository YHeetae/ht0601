#pragma once
#include "Monster.h"
class ScissorKing : public Monster
{
	char* scissorKingName;
	int scissorKingHp;
	char scissorKingResult[6];
public:
	ScissorKing(char *myname);
	~ScissorKing();
	void attack();
	void lose();
	char* getResult();
	char* getName();
};

