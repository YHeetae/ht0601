#pragma warning(disable : 4996)

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "Monster.h"
#include "ScissorKing.h"
#include "RockKing.h"
#include "PaperKing.h"
using namespace std;

int main() {

	int ranM;	// 랜덤 몬스터 생성난수
	int select;		// 사용자의 가위바위보 선택

	Monster monster;
	cout << "몬스터를 생성합니다." << endl;
	ranM = rand() % 3 + 1;
	if (ranM == 1) {
		char inName[10] = "가위킹";
		ScissorKing Monsters(inName);
		monster = Monsters;
	}
	else if (ranM == 2) {
		char inName[10] = "바위킹";
		RockKing Monsters(inName);
		monster = Monsters;
	}
	else if (ranM == 3) {
		char inName[10] = "보킹";
		PaperKing Monsters(inName);
		monster = Monsters;
	}
	
	cout << monster.getName() << "이 생성되었습니다." << endl;
	/*
	while (1) {
		cout << "[ 1 : 가위 / 2 : 바위 / 3 : 보 ] 선택하시오 --> ";
		cin >> select;
		
		if (select == 1) {
			
		}
	}
	*/

	system("pause");
}