#pragma once
#include <iostream>
#include <cstdio>
#include <cstdlib>

class MyHero
{
private:
	char name[10];
	int age;
	char weapontype[10];
	int level;
	char gender[3];
	int hp;
	int attackPower;
	int defencePower;
public:
	MyHero(char myname[10], int myage, char myweapon[10], char mygender[3], int myAttack, int myDefence);
	
	void walk();
	void run();
	void fly();
	void attack(MyHero otherHero);
	void rest();
	void eat();
	void levelup();
	void death();
	void hit(MyHero otherHero);

	~MyHero();
};

