#pragma once
#include "Monster.h"
class PaperKing : public Monster
{
	char* paperKingName;
	int paperKingHp;
	char paperKingResult[6];
public:
	PaperKing(char *myname);
	~PaperKing();
	void attack();
	void lose();
	char* getResult();
	char* getName();
};

