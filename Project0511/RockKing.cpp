#pragma warning(disable : 4996)

#include "RockKing.h"
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>

RockKing::RockKing(char* myname)
{
	rockKingname = new char[10];
	for (int i = 0; i < strlen(myname); i++) {
		strcpy(&rockKingname[i], &myname[i]);
	}
	rockKingHp = 100;
}

RockKing::~RockKing()
{
	delete[] rockKingname;
}

void RockKing::attack() {
	srand(time(NULL));
	int ran = rand() % 100 + 1;
	if (0 < ran && 50 > ran) {
		char input[6] = "바위";
		for (int i = 0; i < 6; i++) {
			strcpy(&rockKingResult[i], &input[i]);
		}
	}
	else if (50 <= ran && 75 > ran) {
		char input[6] = "가위";
		for (int i = 0; i < 6; i++) {
			strcpy(&rockKingResult[i], &input[i]);
		}
	}
	else if (75 <= ran && 101 > ran) {
		char input[6] = "보";
		for (int i = 0; i < 6; i++) {
			strcpy(&rockKingResult[i], &input[i]);
		}
	}
}
void RockKing::lose() {
	rockKingHp = rockKingHp - 20;
}
char* RockKing::getResult() {
	return rockKingResult;
}
char* RockKing::getName() {
	return rockKingname;
}