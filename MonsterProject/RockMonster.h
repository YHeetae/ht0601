#pragma once
#include "Monster.h"
class RockMonster : public Monster
{

public:
	RockMonster();
	RockMonster(char* myname);
	~RockMonster();
	void attack();

};