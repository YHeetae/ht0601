#pragma once
#include "Monster.h"

class ScissorMonster : public Monster
{
public:
	ScissorMonster();
	ScissorMonster(char* myname);
	~ScissorMonster();
	void attack();
};

