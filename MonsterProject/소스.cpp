#pragma warning(disable : 4996)

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "Monster.h"
#include "RockMonster.h"
#include "ScissorMonster.h"
#include "PaperMonster.h"

using namespace std;



int main() {

	int select;
	int myHp = 100;
	int win = 0;
	int lose = 0;
	int same = 0;
	char inName[7] = "몬스터";
	char rockName[7] = "바위몬";
	char scissorName[7] = "가위몬";
	char paperName[7] = "보몬";

	Monster* monster = new Monster();

	srand(time(NULL));

	cout << "몬스터 생성중......" << endl;
	int ranM = rand() % 3 + 1;
	if (ranM == 1) {
		ScissorMonster* inMonster = new ScissorMonster(scissorName);
		monster = inMonster;
	}
	else if (ranM == 2) {
		RockMonster* inMonster = new RockMonster(rockName);
		monster = inMonster;
	}
	else if (ranM == 3) {
		PaperMonster* inMonster = new PaperMonster(paperName);
		monster = inMonster;
	}
	cout << monster->getName() << "이 생성되었습니다." << endl;

	while (1) {
		cout << "1 : 가위 / 2 : 바위 / 3 : 보 -> ";
		cin >> select;

		if (select == 1) {
			monster->attack();
			if (strcmp(monster->getResult(), "가위") == 0) {
				cout << "비겼습니다." << endl;
				same++;
			}
			else if (strcmp(monster->getResult(), "바위") == 0) {
				cout << "졌습니다. HP가 20 감소됩니다." << endl;
				myHp = myHp - 20;
				lose++;
				if (myHp <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 패배하였습니다." << endl;
					break;
				}
			}
			else if (strcmp(monster->getResult(), "보") == 0) {
				cout << "이겼습니다. 몬스터의 HP가 20 감소됩니다." << endl;
				monster->lose();
				win++;
				if (monster->getHp() <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 승리하였습니다." << endl;
					break;
				}
			}

			cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
		}
		else if (select == 2) {
			monster->attack();
			if (strcmp(monster->getResult(), "바위") == 0) {
				cout << "비겼습니다." << endl;
				same++;
			}
			else if (strcmp(monster->getResult(), "보") == 0) {
				cout << "졌습니다. HP가 20 감소됩니다." << endl;
				myHp = myHp - 20;
				lose++;
				if (myHp <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 패배하였습니다." << endl;
					break;
				}
			}
			else if (strcmp(monster->getResult(), "가위") == 0) {
				cout << "이겼습니다. 몬스터의 HP가 20 감소됩니다." << endl;
				monster->lose();
				win++;
				if (monster->getHp() <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 승리하였습니다." << endl;
					break;
				}
			}

			cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
		}
		else if (select == 3) {
			monster->attack();
			if (strcmp(monster->getResult(), "보") == 0) {
				cout << "비겼습니다." << endl;
				same++;
			}
			else if (strcmp(monster->getResult(), "가위") == 0) {
				cout << "졌습니다. HP가 20 감소됩니다." << endl;
				myHp = myHp - 20;
				lose++;
				if (myHp <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 패배하였습니다." << endl;
					break;
				}
			}
			else if (strcmp(monster->getResult(), "바위") == 0) {
				cout << "이겼습니다. 몬스터의 HP가 20 감소됩니다." << endl;
				monster->lose();
				win++;
				if (monster->getHp() <= 0) {
					cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
					cout << "게임에서 승리하였습니다." << endl;
					break;
				}
			}

			cout << "나의 HP : " << myHp << " / 몬스터의 HP : " << monster->getHp() << endl;
		}
	}

	cout << win << "승 " << same << "무 " << lose << "패" << endl;

	system("pause");
}



