#pragma once
class Monster
{
protected:
	char* name;
	int hp;
	char result[5];
public:
	Monster();
	Monster(char* myname);
	~Monster();
	void attack();
	void lose();
	char* getName();
	char* getResult();
	int getHp();
};

