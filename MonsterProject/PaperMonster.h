#pragma once
#include "Monster.h"
class PaperMonster : public Monster
{
public:
	PaperMonster();
	PaperMonster(char* myname);
	~PaperMonster();
	void attack();
};

