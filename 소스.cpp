#pragma warning(disable : 4996)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

class Student {
private:
	char name[7];
	double kor;
	double eng;
	double math;
	double avg;
public:
	void inputData();
	void printData(FILE *fp);
	~Student();
};

void main() {

	FILE *fp;
	Student std;
	int choose;

	fp = fopen("score.txt", "wt");
	if (fp == NULL) {
		printf(" 에러 : 존재하지 않는 파일입니다.\n");
		system("pause");
		exit(1);
	}

	while (1) {
		std.inputData();
		std.printData(fp);
		cout << "계속 입력 : 1 / 종료 : 2";
		cin >> choose;
		if (choose == 2) {
			break;
		}
	}


	system("pause");
}

void Student::inputData() {
	cout << "학생의 이름을 입력하시오 : ";
	cin >> name;
	cout << "국어의 점수를 입력하시오 : ";
	cin >> kor;
	cout << "영어의 점수를 입력하시오 : ";
	cin >> eng;
	cout << "수학의 점수를 입력하시오 : ";
	cin >> math;
	avg = (kor + eng + math) / 3;
}

void Student::printData(FILE *fp) {
		fprintf(fp, "이름 : %s / 국어 : %.1lf / 영어 : %.1lf / 수학 : %.1lf / 평균 : %.1lf\n", name, kor, eng, math, avg);
}

Student::~Student() {

}


